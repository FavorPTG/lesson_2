package com.example.accumulationcalculation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.io.IOError;

public class MainActivity extends AppCompatActivity {

    private int calculation(int summ, int money, int income, int percent){
        int res = money;
        int mounth = 0;

        while(res < summ){

            System.out.println(mounth);

            if(percent != 0)
                res += (res * (100 + percent) / 100) / 12;

            res += income;
            mounth++;
            System.out.println(res);
        }

        return mounth;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView result = findViewById(R.id.result);
        TextInputEditText summ = findViewById(R.id.summ);
        TextInputEditText money = findViewById(R.id.money);
        TextInputEditText income = findViewById(R.id.income);
        TextInputEditText percent = findViewById(R.id.percent);
        Button calculate = findViewById(R.id.button);

        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int smm = Integer.parseInt(String.valueOf(summ.getText()));
                int mon = Integer.parseInt(String.valueOf(money.getText()));
                int inc = Integer.parseInt(String.valueOf(income.getText()));
                int perc = Integer.parseInt(String.valueOf(percent.getText()));

                result.setText("Результат:\n" + String.valueOf(calculation(smm, mon, inc, perc)) + " месяцев");
            }
        });
    }
}